#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
NOCOLOR='\033[0m'
bold=$(tput bold)
normal=$(tput sgr0)

echo -e "$GREEN"
echo -e "Automatischer download der Bibliotheken wird vorbereitet..."
sleep 2

echo -e "$NOCOLOR"
mkdir BITS1_Bibliotheken
echo -e "$GREEN"
echo -e "Ordner BITS1_Bibliotheken wurde erfolgreich erstellt."
echo -e " "
sleep 1
cd BITS1_Bibliotheken

echo -e "Starte download..."
sleep 1
echo -e "$NOCOLOR"

# BITS-i Bibliotheken
git clone https://git-ce.rwth-aachen.de/bits_libs/bits1-libraries.git


echo -e "$GREEN"
echo -e "Alle Bibliotheken wurden erfolgreich "
echo -e "in den Ordner BITS-i_Bibliotheken gedownloadet."
echo -e "$RED"
echo -e "__________________________________________________________________"
echo -e "__________________________________________________________________"
echo -e " "
echo -e "\033[31;7m${bold} !!!WICHTIG!!! \033[0m"
echo -e " "
echo -e "\033[31;7m${bold}Bitte kopiert alle Ordner aus BITS1_Bibliotheken in den\033[0m"
echo -e "\033[31;7m${bold}Arduino Libraries Ordner!\033[0m"
echo -e "$RED"
echo -e "__________________________________________________________________"
echo -e "__________________________________________________________________"
echo -e " "

echo -e "$GREEN"
echo -e "Wenn die Bibliotheken erfolgreich in den Arduino Libraries Ordner" 
echo -n "kopiert wurden,"
read -p " mit beliebiger Taste fortfahren" x
echo -e " "
